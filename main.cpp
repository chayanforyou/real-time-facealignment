#include <iostream>
#include <cstdio>
#include <cassert>
#include <vector>
#include <string>


#include <include/common.hpp>
#include <include/testing.hpp>




// Main
int main(int argc, char *argv[])
{
    std::cout << "Starting face alignment evaluation process...." << std::endl;

    /// Path to database folder
    std::string database_folder = "../data";
    std::string models_folder = "../models";


    //********************************//
    //   TEST AND TRAINING FUNCTIONS  //
    //********************************//

    if (strcmp(argv[1], "ert_300W_test") == 0)
    {
        /// EXAMPLE ///
        /// ert_300W_test shape_predictor_68_face_landmarks.dat fullset_test.xml 0.08

        testing tester = testing();
        std::string name_model = models_folder + "/" + argv[2];  // Model name root
        std::string dataset_file = argv[3];                      // Txt file with the images.
        float threshold = static_cast<float>(atof(argv[4]));     // Error threshold

        std::cout << " - 300W Benchmarks for ERT models in Dlib. " << std::endl;
        std::cout << " - Input arguments: " << std::endl;
        std::cout << "     - Model name root: " << name_model << std::endl;
        std::cout << "     - Dataset file: " << dataset_file << std::endl;
        std::cout << "     - Error threshold: " << threshold << std::endl;
        return tester.ert_300W_test(name_model, database_folder, dataset_file, threshold);

    }


    if (strcmp(argv[1], "ert_jitter_test") == 0)
    {
        /// EXAMPLE ///
        /// ert_jitter_test shape_predictor_68_face_landmarks.dat jittering_test.txt 0.08

        testing tester = testing();
        std::string name_model = models_folder + "/" + argv[2];  // Model name root
        std::string dataset_file = argv[3];                      // Txt file with the images in Jittering benchmark
        float threshold = static_cast<float>(atof(argv[4]));     // Error threshold

        std::cout << " - Jitter Benchmark. " << std::endl;
        std::cout << " - Input arguments: " << std::endl;
        std::cout << "     - Model name root: " << name_model << std::endl;
        std::cout << "     - Dataset file: " << dataset_file << std::endl;
        std::cout << "     - Error threshold: " << threshold << std::endl;
        return tester.ert_jittering_test(name_model, database_folder, dataset_file, threshold);

    }



    if (strcmp(argv[1], "lbfocv_300W_test") == 0)
    {
        /// EXAMPLE ///
        /// lbfocv_300W_test default_opencv_lfbmodel.yaml fullset_test.txt 0.08

        testing tester = testing();
        std::string name_model = models_folder + "/" + argv[2];  // Model name root
        std::string dataset_file = argv[3];                      // Txt file with the images in Jittering benchmark
        float threshold = static_cast<float>(atof(argv[4]));     // Error threshold

        std::cout << " - 300W benchmarks for LBF models in OpenCV. " << std::endl;
        std::cout << " - Input arguments: " << std::endl;
        std::cout << "     - Model name root: " << name_model << std::endl;
        std::cout << "     - Dataset file: " << dataset_file << std::endl;
        std::cout << "     - Error threshold: " << threshold << std::endl;
        return tester.lbf_ocv_300W_test(name_model, database_folder, dataset_file, threshold);

    }



    if (strcmp(argv[1], "lbf_jitter_test") == 0)
    {

        /// EXAMPLE ///
        /// lbf_jitter_test default_opencv_lfbmodel.yaml jittering_test.txt 0.08

        testing tester = testing();
        std::string name_model = models_folder + "/" + argv[2];  // Model name root
        std::string dataset_file = argv[3];                      // Txt file with the images in Jittering benchmark
        float threshold = static_cast<float>(atof(argv[4]));     // Error threshold

        std::cout << " - Jitter Benchmark. " << std::endl;
        std::cout << " - Input arguments: " << std::endl;
        std::cout << "     - Model name root: " << name_model << std::endl;
        std::cout << "     - Dataset file: " << dataset_file << std::endl;
        std::cout << "     - Error threshold: " << threshold << std::endl;
        return tester.lbf_jittering_test(name_model, database_folder, dataset_file, threshold);

    }


    if (strcmp(argv[1], "ert_video_test") == 0)
    {
        /// EXAMPLE ///
        /// ert_video_test shape_predictor_68_face_landmarks.dat ../data/videos/vid.avi

        testing tester = testing();
        std::string name_model = models_folder + "/" + argv[2];  // Model name root
        std::string video_name = argv[3];                      // Txt file with the images in Jittering benchmark

        std::cout << " - ERT models video test. " << std::endl;
        std::cout << " - Input arguments: " << std::endl;
        std::cout << "     - Model name root: " << name_model << std::endl;
        std::cout << "     - Video file: " << video_name << std::endl;
        return tester.ert_video_test(name_model, video_name);
    }


    if (strcmp(argv[1], "lbf_video_test") == 0)
    {
        /// EXAMPLE ///
        /// lbf_video_test default_opencv_lfbmodel.yaml ../data/videos/vid.avi

        testing tester = testing();
        std::string name_model = models_folder + "/" + argv[2];  // Model name root
        std::string video_name = argv[3];                      // Txt file with the images in Jittering benchmark

        std::cout << " - LBF models video test." << std::endl;
        std::cout << " - Input arguments: " << std::endl;
        std::cout << "     - Model name root: " << name_model << std::endl;
        std::cout << "     - Video file: " << video_name << std::endl;
        return tester.lbf_video_test(name_model, video_name);

    }


    if (strcmp(argv[1], "dummy") == 0)
    {
        return EXIT_SUCCESS;
    }else{
        std::cout << "Wrong Arguments." << std::endl;
        return EXIT_FAILURE;
    }
}


