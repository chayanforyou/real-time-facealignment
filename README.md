# Real-time face alignment: evaluation methods, training strategies and implementation optimization


This is a reference implementation and a set of models for real-time face alignment, to be directly integrated in Dlib or OpenCV libraries.
This is a public repository intended for the general public.

If you use our models, please cite our journal article: 

Álvarez Casado C. & Bordallo López M. "Real-time face alignment: evaluation methods, training strategies and implementation optimization SPRINGER JOURNAL OF REAL-TIME IMAGE PROCESSING (2021) 
[Download PDF](https://drive.google.com/file/d/1EArRdebyuLi1SPin9xauTMdY1At7teXS/view?usp=sharing) 

Authors:
- Constantino Álvarez Casado
- Miguel Bordallo López


![Kiku](docs/images/quality_results_frontal.png)


## Download our improved Models
 - Download the models evaluated and trained for the manuscript in: [Our Models](https://drive.google.com/drive/folders/1t1fRQfTaL1-XgGA1JSzuvLSXsitZ6Scj?usp=sharing)
 - Quantized models: Ask to the authors!

## Download the original Models for comparison purposes
 - Original and default Dlib ERT model: [Dlib ERT Model](https://github.com/davisking/dlib-models)
 - Original and default OpenCV LBF model: [OpenCV LBF Model](https://raw.githubusercontent.com/kurnianggoro/GSOC2017/master/data/lbfmodel.yaml)
 - Original DAN models: [DAN Model](https://drive.google.com/drive/folders/168tC2OxS5DjyaiuDy_JhIV3eje8K_PLJ)
 - OpenCV models to use the DNN Face Detector: [OpenCV DNN Face Detector Model](https://github.com/spmallick/learnopencv/tree/master/FaceDetectionComparison/models)

## Download benchmarks testsets
In the folder data of the repository, we provide the ground truth annotations in .txt or .xml files to ease the tests, but they can be downloaded from
and read from the original datasets.
 - Original 300W benchmarks: [300W databases](https://ibug.doc.ic.ac.uk/resources/facial-point-annotations/)
 - Private benchmarks (Own databases): 
    - Jitter benchmark 1: [Jitter benchmark](https://drive.google.com/drive/folders/1o5qMIVMQcnvlufn1vLPi9WF7-h0id41l?usp=sharing)
    - For other benchmarks: Ask the authors!

## Check some demo videos in:
 - ERT models comparison in Dlib: [ERT Models comparison](https://www.youtube.com/watch?v=U3jx43uuAGg)
 - LBF models comparison in OpenCV: [LBF Models comparison](https://www.youtube.com/watch?v=Zyv3VUHrLgQ)
 - LBF models comparison in own implementation: [LBF Models comparison Own](https://www.youtube.com/watch?v=ELQptvXR3R4)

## Installation

If you want to use our test and evaluation application, clone the repository and follow the instructions.

### Requirements

* C++11
* Linux (and Windows<sup>[1](#myfootnote1)</sup>)
* OpenCV 4.1.0+
* Dlib 19.21+
* CMake 3.10.0+

<a name="myfootnote1">1</a>: *Not tested regularly on Windows. Fully tested in Linux Ubuntu O.S.*

### Usage

Build and run.

To build the application from source perform the following:
```
mkdir build && cd build
cmake ..
cmake --build . 
```

To run the application:

- ERT 300W benchmarks. 
  - Example arguments: `ert_300W_test shape_predictor_68_face_landmarks.dat common_subset_test.xml 0.08`

- ERT jittering benchmarks. 
  - Example arguments: `ert_jitter_test shape_predictor_68_face_landmarks.dat jittering_test.txt 0.08`

- LBF 300W Benchmarks using OpenCV format. 
  - Example arguments: `lbfocv_300W_test default_opencv_lbfmodel.yalm common_subset_test.txt 0.08`

- LBF jittering benchmarks. 
  - Example arguments: `lbf_jitter_test default_opencv_lfbmodel.yaml jittering_test.txt 0.08`

- ERT Video/Webcam test. 
  - Example arguments: `ert_video_test shape_predictor_68_face_landmarks.dat ../data/videos/vid.avi`

- LBF Video/Webcam test. 
  - Example arguments: `lbf_video_test default_opencv_lfbmodel.yaml ../data/videos/vid.avi`


## Citation

If you use this software in your research, then please cite the following:

Álvarez Casado C. & Bordallo López M.  (2021) Real-time face alignment: evaluation methods, training strategies and implementation optimization SPRINGER JOURNAL OF REAL-TIME IMAGE PROCESSING (in press) [Download PDF](https://drive.google.com/file/d/1EArRdebyuLi1SPin9xauTMdY1At7teXS/view?usp=sharing) 


## License

While the code is licensed under the MIT license, which allows for commercial use, keep in mind that the models linked 
above were trained on the 300-W dataset, which allows for research use only. 
For details please see: https://ibug.doc.ic.ac.uk/resources/facial-point-annotations/

Also, private datasets and benchmarks cannot be used or distributed without permission from the authors. 
