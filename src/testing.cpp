#include <iostream>
#include <string>


#include <dlib/opencv.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/image_io.h>
#include <dlib/image_keypoint.h>
#include <dlib/console_progress_indicator.h>
#include <dlib/data_io.h>
#include <dlib/statistics.h>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/video/tracking.hpp>

#include <opencv2/calib3d/calib3d.hpp>
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/core/utility.hpp"

#ifndef DLIB
#include <opencv2/face.hpp>
#endif


#include "include/testing.hpp"
#include "include/common.hpp"


// Variables related to OpenCV DNN Face detector
const size_t inWidth = 300;
const size_t inHeight = 300;
const double inScaleFactor = 1.0;
const float confidenceThreshold = 0.7f;
const cv::Scalar meanVal(104.0, 177.0, 123.0);




testing::testing(){}


int testing::ert_300W_test(std::string name_model, std::string database_folder, std::string test_file, float threshold)
{
    dlib::array<dlib::array2d<unsigned char> > images_test;
    std::vector<std::vector<dlib::full_object_detection> > faces_test;

    std::cout << " - Starting to read the images..." << std::endl;
    std::vector<std::string> parts_list;
    load_image_dataset(images_test, faces_test, database_folder+ "/" + test_file, parts_list);
    std::cout << " - Number of test images read: " <<images_test.size() << std::endl;
    std::cout << " - Number of test objects read: " <<faces_test.size() << std::endl;

    dlib::shape_predictor sp;
    dlib::deserialize(name_model) >> sp;



    std::cout << " - Mean test error for "<< test_file << ": "<< std::endl;
    double initTime = vh::CLOCK();
    double error = test_shape_predictor(sp, images_test, faces_test, vh::get_interocular_distances_ert(faces_test));
    std::cout << "    - Error: "<< error << std::endl;
    double currentTime = vh::CLOCK();
    std::cout << "Process time: " << currentTime - initTime << " ms." << std::endl;

    return EXIT_SUCCESS;
}



int testing::ert_jittering_test(std::string name_model, std::string database_folder, std::string test_file, float threshold)
{

    std::cout << " - Starting to read model..." << std::endl;
    vh::Config &config = vh::Config::GetInstance();
    dlib::shape_predictor sp;
    dlib::deserialize(name_model) >> sp;

    std::cout << " - Starting to read the images..." << std::endl;
    std::string dataset = database_folder+ "/" + test_file;
    std::vector<cv::Mat> imgs, imgs_color,  gt_shapes;
    std::vector<std::string> imgsNames;
    std::vector<vh::FaceBox> bboxes;

    vh::loadDatasetJittering(dataset, imgs, imgs_color, gt_shapes, bboxes, false);

    size_t N = imgs.size();
    std::vector<cv::Mat> current_shapes(N);

    double initTime = vh::CLOCK();
    for (size_t i = 0; i < N; i++) {
        dlib::cv_image<dlib::bgr_pixel> cimg_big(imgs_color[i]);
        dlib::full_object_detection shapeIn = sp(cimg_big, dlib::rectangle(bboxes[i].x, bboxes[i].y, bboxes[i].x + bboxes[i].width, bboxes[i].y + bboxes[i].height));
        //std::vector<cv::Point> landmarks = get68LandMarks(shapeIn);

        current_shapes[i] = cv::Mat::zeros(68, 2, CV_64FC1);
        for (int j = 0; j < 68; j++) {
            current_shapes[i].at<double>(j, 0) = shapeIn.part(j).x();
            current_shapes[i].at<double>(j, 1) = shapeIn.part(j).y();
        }

    }

    double error_jitter = vh::jitterErrorMeasurement(current_shapes);

    double currentTime = vh::CLOCK();
    std::cout << "Process time for the jittering benchmark(" << N << " images): " << currentTime - initTime << " ms. (" << N*1000/(currentTime - initTime) << "fps.)" << std::endl;

    return EXIT_SUCCESS;
}



int testing::lbf_ocv_300W_test(std::string name_model, std::string database_folder, std::string test_file, float threshold)
{

    // Create an instance of Facemark
    cv::face::FacemarkLBF::Params parameters;
    parameters.verbose = false;
    cv::Ptr<cv::face::Facemark> facemark = cv::face::FacemarkLBF::create(parameters);


    // Load landmark detector
    facemark->loadModel(name_model);


    std::string dataset = database_folder+ "/" + test_file;
    std::vector<cv::Mat> imgs, imgs_color,  gt_shapes;
    std::vector<std::string> imgsNames;
    std::vector<vh::FaceBox> bboxes;

    std::cout << " - Starting to read the images..." << std::endl;
    vh::loadDataset(dataset, imgs, imgs_color, gt_shapes, bboxes, false);


    size_t N = imgs.size();
    std::vector<cv::Mat> predicted_shapes(N);
    std::vector<cv::Rect> faces;
    std::vector< std::vector<cv::Point2f> > landmarks;
    cv::Mat gray;
    double total_time = 0;


    for (size_t i = 0; i < N; i++)
    {
        faces.push_back(cv::Rect(bboxes[i].x, bboxes[i].y, bboxes[i].width, bboxes[i].height));

        double initTime = vh::CLOCK();
        bool success = facemark->fit(imgs_color[i], faces, landmarks);
        predicted_shapes[i] = cv::Mat::zeros(landmark_n, 2, CV_64FC1);

        for (size_t j = 0; j < landmark_n; j++)
        {
            predicted_shapes[i].at<double>(j, 0) = landmarks[0].at(j).x;
            predicted_shapes[i].at<double>(j, 1) = landmarks[0].at(j).y;
        }

        double currentTime = vh::CLOCK();
        std::cout << "Process time[" << i << "]: " << currentTime - initTime << "ms." << std::endl;
        total_time += currentTime - initTime;

        faces.clear();
        landmarks.clear();
    }

    double error = vh::get_mean_error(gt_shapes, predicted_shapes);
    double accuracy = vh::get_success_rate(gt_shapes, predicted_shapes, threshold);
    std::cout << "    - Error: "<< error << "( Success rate for threshold of " << threshold*100 <<"%: "<< accuracy << std::endl;
    std::cout << "Process time: " << total_time << " ms. (" << N*1000/ total_time  << " fps.)" << std::endl;

    return EXIT_SUCCESS;
}



int testing::lbf_jittering_test(std::string name_model, std::string database_folder, std::string test_file, float threshold)
{
    // Create an instance of Facemark
    cv::face::FacemarkLBF::Params parameters;
    parameters.verbose = false;
    cv::Ptr<cv::face::Facemark> facemark = cv::face::FacemarkLBF::create(parameters);


    // Load landmark detector
    std::cout << " - Starting to read model..." << std::endl;
    facemark->loadModel(name_model);


    std::string dataset = database_folder+ "/" + test_file;
    std::vector<cv::Mat> imgs, imgs_color,  gt_shapes;
    std::vector<std::string> imgsNames;
    std::vector<vh::FaceBox> bboxes;

    std::cout << " - Starting to read the images..." << std::endl;
    vh::loadDatasetJittering(dataset, imgs, imgs_color, gt_shapes, bboxes, false);


    size_t N = imgs.size();
    std::vector<cv::Mat> predicted_shapes(N);
    std::vector<cv::Rect> faces;
    std::vector< std::vector<cv::Point2f> > landmarks;
    cv::Mat gray;
    double total_time = 0;

    double initTime = vh::CLOCK();
    for (size_t i = 0; i < N; i++)
    {
        faces.push_back(cv::Rect(bboxes[i].x, bboxes[i].y, bboxes[i].width, bboxes[i].height));

        double initTime = vh::CLOCK();
        bool success = facemark->fit(imgs_color[i], faces, landmarks);
        predicted_shapes[i] = cv::Mat::zeros(landmark_n, 2, CV_64FC1);

        for (size_t j = 0; j < landmark_n; j++)
        {
            predicted_shapes[i].at<double>(j, 0) = landmarks[0].at(j).x;
            predicted_shapes[i].at<double>(j, 1) = landmarks[0].at(j).y;
        }

        double currentTime = vh::CLOCK();
        std::cout << "Process time[" << i << "]: " << currentTime - initTime << "ms." << std::endl;
        total_time += currentTime - initTime;

        faces.clear();
        landmarks.clear();
    }


    double error_jitter = vh::jitterErrorMeasurement(predicted_shapes);

    double currentTime = vh::CLOCK();
    std::cout << "Process time for the jittering benchmark(" << N << " images): " << currentTime - initTime << " ms. (" << N*1000/(currentTime - initTime) << "fps.)" << std::endl;

    return EXIT_SUCCESS;
}




// Function copied from https://github.com/spmallick/learnopencv/blob/master/FaceDetectionComparison/face_detection_opencv_dnn.cpp
cv::Rect detectFaceOpenCVDNN(cv::dnn::Net net, cv::Mat &frameOpenCVDNN, std::string framework)
{
    int frameHeight = frameOpenCVDNN.rows;
    int frameWidth = frameOpenCVDNN.cols;
    cv::Rect out_face_rect;

    cv::Mat inputBlob;
    if (framework == "caffe")
        inputBlob = cv::dnn::blobFromImage(frameOpenCVDNN, inScaleFactor, cv::Size(inWidth, inHeight), meanVal, false, false);
    else
        inputBlob = cv::dnn::blobFromImage(frameOpenCVDNN, inScaleFactor, cv::Size(inWidth, inHeight), meanVal, true, false);

    net.setInput(inputBlob, "data");
    cv::Mat detection = net.forward("detection_out");

    cv::Mat detectionMat(detection.size[2], detection.size[3], CV_32F, detection.ptr<float>());

    for(int i = 0; i < detectionMat.rows; i++)
    {
        float confidence = detectionMat.at<float>(i, 2);

        if(confidence > confidenceThreshold)
        {
            int x1 = static_cast<int>(detectionMat.at<float>(i, 3) * frameWidth);
            int y1 = static_cast<int>(detectionMat.at<float>(i, 4) * frameHeight);
            int x2 = static_cast<int>(detectionMat.at<float>(i, 5) * frameWidth);
            int y2 = static_cast<int>(detectionMat.at<float>(i, 6) * frameHeight);
            out_face_rect = cv::Rect(x1,y1, x2-x1, y2-y1);
            //cv::rectangle(frameOpenCVDNN, cv::Point(x1, y1), cv::Point(x2, y2), cv::Scalar(0, 255, 0),2, 4);
        }
    }

    return out_face_rect;

}


// The face alignment models are trained using square rects.
cv::Rect adaptRect4FacialLandmarks(cv::Rect input_rect){

    cv::Rect output_rect;

    int y_center = input_rect.y + input_rect.height/2;
    int new_y = y_center - input_rect.width/2.5;
    output_rect = cv::Rect(input_rect.x, new_y, input_rect.width, input_rect.width);

    return output_rect;
}


int testing::ert_video_test(std::string name_model, std::string video_name)
{
    std::string framework = "";
    std::cout << " - Starting to read model..." << std::endl;
    vh::Config &config = vh::Config::GetInstance();
    dlib::shape_predictor sp;
    dlib::deserialize(name_model) >> sp;


    const std::string tensorflowConfigFile = "../models/opencv_face_detector.pbtxt";
    const std::string tensorflowWeightFile = "../models/opencv_face_detector_uint8.pb";

    cv::dnn::Net net = cv::dnn::readNetFromTensorflow(tensorflowWeightFile, tensorflowConfigFile);


    std::cout << " - Starting to open video..." << std::endl;
    cv::VideoCapture cap(video_name); // open the default camera
    if(!cap.isOpened())  // check if we succeeded
        return EXIT_FAILURE;


    cv::Mat frame;
    cap >> frame;
    int codec = cv::VideoWriter::fourcc('M', 'P', '4', 'V');  // select desired codec (must be available at runtime)
    double fps = 25.0;                          // framerate of the created video stream
    cv::VideoWriter video("output.mp4", codec, fps, cv::Size(frame.cols, frame.rows));

    cv::namedWindow("output video", cv::WINDOW_NORMAL);

    size_t i = 0;
    size_t j = 0;

    for(;;)
    {
        // Capturing frame.
        cap >> frame; // get a new frame from camera
        if (frame.empty())
                    break;
        // Rsize + Rotation
        //cv::resize(frame, frame, cv::Size(frame.cols/2, frame.rows/2));
        //cv::rotate(frame, frame, cv::ROTATE_90_COUNTERCLOCKWISE);

        // Face detection with DNN
        cv::Rect out_rect = detectFaceOpenCVDNN(net, frame, framework);

        // Adapt to squared rect.
        out_rect = adaptRect4FacialLandmarks(out_rect);

        dlib::cv_image<dlib::bgr_pixel> cimg_big(frame);
        double initTime = vh::CLOCK();
        dlib::full_object_detection shapeIn = sp(cimg_big, dlib::rectangle(out_rect.x, out_rect.y, out_rect.x + out_rect.width, out_rect.y + out_rect.height));
        double currentTime = vh::CLOCK();
        std::cout << "Process time for computing landmark points: " << currentTime - initTime << " ms. (" << 1000/(currentTime - initTime) << "fps.)" << std::endl;


        cv::rectangle(frame, out_rect, cv::Scalar(0, 255, 0), 2, 4);
        for (int j = 0; j < 68; j++)
        {
            cv::circle(frame, cv::Point(shapeIn.part(j).x(),shapeIn.part(j).y()), 2, cv::Scalar(255,255,255), -1);
        }
        cv::putText(frame, " - Algorithm: ERT", cv::Point(5,30), 3, 1.0, cv::Scalar(255,255,255), 2);
        cv::putText(frame, " - Model: shape_predictor_68_face_landmarks.dat (dlib default)", cv::Point(5,60), 3, 1.0, cv::Scalar(255,255,255), 2);
        cv::putText(frame, " - Video: 300-VW", cv::Point(5,90), 3, 1.0, cv::Scalar(255,255,255), 2);
        cv::putText(frame, " - Fps: " + std::to_string(int(1000/(currentTime - initTime))), cv::Point(5,120), 3, 1.0, cv::Scalar(255,255,255), 2);


        video.write(frame);

        // Show resulted frame.
        cv::imshow("output video", frame);
        char key = static_cast<char>(cv::waitKey(10));
        if(key == 27)
            break;
    }

    return EXIT_SUCCESS;
}




int testing::lbf_video_test(std::string name_model, std::string video_name)
{
    std::string framework = "";
    std::cout << " - Starting to read model..." << std::endl;
    vh::Config &config = vh::Config::GetInstance();
    // Create an instance of Facemark
    cv::face::FacemarkLBF::Params parameters;
    parameters.verbose = false;
    cv::Ptr<cv::face::Facemark> facemark = cv::face::FacemarkLBF::create(parameters);


    // Load face  and landmark detector
    std::cout << " - Starting to read models..." << std::endl;
    facemark->loadModel(name_model);
    const std::string tensorflowConfigFile = "../models/opencv_face_detector.pbtxt";
    const std::string tensorflowWeightFile = "../models/opencv_face_detector_uint8.pb";
    cv::dnn::Net net = cv::dnn::readNetFromTensorflow(tensorflowWeightFile, tensorflowConfigFile);


    std::cout << " - Starting to open video..." << std::endl;
    cv::VideoCapture cap(video_name); // open the default camera
    if(!cap.isOpened())  // check if we succeeded
        return EXIT_FAILURE;

    cv::Mat frame;
    cap >> frame;
    int codec = cv::VideoWriter::fourcc('M', 'P', '4', 'V');  // select desired codec (must be available at runtime)
    double fps = 25.0;                          // framerate of the created video stream
    cv::VideoWriter video("output.mp4", codec, fps, cv::Size(frame.cols, frame.rows));

    cv::namedWindow("output video", cv::WINDOW_NORMAL);
    size_t i = 0;
    size_t j = 0;
    std::vector<cv::Rect> faces;
    std::vector< std::vector<cv::Point2f> > landmarks;

    for(;;)
    {
        // Capturing frame.
        cap >> frame; // get a new frame from camera
        if (frame.empty())
                    break;
        // Rsize + Rotation
        //cv::resize(frame, frame, cv::Size(frame.cols/2, frame.rows/2));
        //cv::rotate(frame, frame, cv::ROTATE_90_COUNTERCLOCKWISE);

        // Face detection with DNN
        cv::Rect out_rect = detectFaceOpenCVDNN(net, frame, framework);

        // Adapt to squared rect.
        out_rect = adaptRect4FacialLandmarks(out_rect);
        faces.push_back(out_rect);

        double initTime = vh::CLOCK();
        bool success = facemark->fit(frame, faces, landmarks);
        double currentTime = vh::CLOCK();
        std::cout << "Process time for computing landmark points: " << currentTime - initTime << " ms. (" << 1000/(currentTime - initTime) << "fps.)" << std::endl;

        cv::rectangle(frame, out_rect, cv::Scalar(0, 255, 0), 2, 4);
        for (int j = 0; j < 68; j++)
        {
            cv::circle(frame, cv::Point(landmarks[0].at(j).x, landmarks[0].at(j).y), 2, cv::Scalar(255,255,255), -1);
        }
        cv::putText(frame, " - Algorithm: LBF (OpenCV implementation)", cv::Point(5,30), 3, 1.0, cv::Scalar(255,255,255), 2);
        cv::putText(frame, " - Model: LBF686_GTX.yaml", cv::Point(5,65), 3, 1.0, cv::Scalar(255,255,255), 2);
        cv::putText(frame, " - Model size: 179.8 MB", cv::Point(5,100), 3, 1.0, cv::Scalar(255,255,255), 2);
        cv::putText(frame, " - Video: 300-VW", cv::Point(5,135), 3, 1.0, cv::Scalar(255,255,255), 2);
        cv::putText(frame, " - Fps: " + std::to_string(int(1000/(currentTime - initTime))), cv::Point(5,170), 3, 1.0, cv::Scalar(255,255,255), 2);

        faces.clear();
        landmarks.clear();

        video.write(frame);

        // Show resulted frame.
        cv::imshow("output video", frame);
        char key = static_cast<char>(cv::waitKey(10));
        if(key == 27)
            break;
    }


    return EXIT_SUCCESS;
}


