#ifndef TESTING_H
#define TESTING_H

#include <string>

class testing
{
public:
    testing();
    int ert_300W_test(std::string name_model, std::string database_folder, std::string test_file, float threshold);
    int ert_jittering_test(std::string name_model, std::string database_folder, std::string test_file, float threshold);
    int ert_video_test(std::string name_model, std::string video_name);
    int lbf_ocv_300W_test(std::string name_model, std::string database_folder, std::string test_file, float threshold);
    int lbf_jittering_test(std::string name_model, std::string database_folder, std::string test_file, float threshold);
    int lbf_video_test(std::string name_model, std::string video_name);
};

#endif // TESTING_H
